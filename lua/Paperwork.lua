Paperwork = {}

function Paperwork.init()
	
	--mainMenu.paintBackground()
	--mainMenu.refreshMenu(Perfil)
	Main.state = Detail
	Detail.initDetail(content.paperwork, Paperwork)
	--canvas:flush()

end

function Paperwork.handler(key)

	if (key == 'RED') then
		--Main
		Main.state = Main
		mainMenu[focused]:unsetFocus()
		focused = 1
		mainMenu[focused]:setFocus()
		Main.showInit()
	
	
	elseif (key == 'GREEN') then
		--Paperwork
		Main.state = Paperwork
		mainMenu[focused]:unsetFocus()
		focused = 2
		mainMenu[focused]:setFocus()
		Paperwork.init()
	
	
	--[[elseif (key == 'YELLOW') then
		--Directory
		Main.state = Directory
		mainMenu[focused]:unsetFocus()
		focused = 3
		mainMenu[focused]:setFocus()
		canvas:compose(0,0,images.background)
		Directory.init()
--]]
	elseif (key == 'BLUE') then
		--Calendar
		Main.state = Calendar
		mainMenu[focused]:unsetFocus()
		focused = 4
		mainMenu[focused]:setFocus()
		Calendar.init()

	elseif (key == 'CURSOR_UP') then
			
		if (paperFocused == 1) then 
			Perfil[1]:unsetFocus()
			Perfil[#Perfil]:setFocus()
			paperFocused = #Perfil
				
		else
			Perfil[paperFocused]:unsetFocus()
			Perfil[paperFocused-1]:setFocus()
			paperFocused = paperFocused-1
		
		end
			
	elseif (key == 'CURSOR_DOWN') then
				
		if (paperFocused == #Perfil) then 
			Perfil[#Perfil]:unsetFocus()
			Perfil[1]:setFocus()
			paperFocused = 1
			
		else
			Perfil[paperFocused]:unsetFocus()
			Perfil[paperFocused+1]:setFocus()
			paperFocused = paperFocused+1

		end

	elseif (key == 'ENTER' or key == 'OK') then
			
	end	
	canvas:flush()
end
