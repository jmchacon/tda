
Detail = {}

function Detail.initDetail(var, section)
	Detail.itemsPerPage = 4
	Detail.page = {}	
	Detail.section = section
	Detail.currentPage = 0

	for i=1, math.ceil(#var/Detail.itemsPerPage) do

		Detail.page[i] = {} 
		local pageFirstItem = (i-1)*Detail.itemsPerPage
		local pageFilled = false

		for j = 1, Detail.itemsPerPage do
			if not pageFilled then 
				local contentColor = "red"
				b_text = images.b_green --TEST
				if Detail.section == Paperwork then
					contentColor = "green"
					b_text = images.b_green --TEST
				elseif Detail.section == Directory then
					contentColor = "yellow"
					b_text = images.b_texto2 --TEST
				elseif Detail.section == Calendar then
					contentColor = "blue"
					b_text = images.b_blue --TEST
				end
				Detail.page[i][j] = Label:new():setBounds(var[pageFirstItem+j].site, var[pageFirstItem+j].name, var[pageFirstItem+j].address, var[pageFirstItem+j].phone, var[pageFirstItem+j].leader, images.b_texto,b_text, false, 20, 150, contentColor)
			end
			if pageFirstItem + j == #var then 
				pageFilled = true
			end
		end
	end

	Detail.nextPage()
end

function Detail.nextPage()
	if #Detail.page > Detail.currentPage then
		Detail.currentPage = Detail.currentPage + 1
		
		mainMenu.paintBackgroundDetail()
		Detail.pageItem = {}

		for i=1, #Detail.page[Detail.currentPage] do
			Detail.pageItem[i] = Detail.page[Detail.currentPage][i]
			Detail.pageItem[i].top = (i*64)+110	

			if i == 1 then
				Detail.pageItem[i]:setFocus()
			else 
				Detail.pageItem[i]:unsetFocus()
			end
		end

		Detail.currentPageItem = 1
		Detail.pageItem[Detail.currentPageItem]:setData()
	end
end

function Detail.previousPage()
	if Detail.currentPage > 1 then
		Detail.currentPage = Detail.currentPage - 1

		mainMenu.paintBackgroundDetail()
		Detail.pageItem = {}

		for i=1, #Detail.page[Detail.currentPage] do
			Detail.pageItem[i] = Detail.page[Detail.currentPage][i]
			Detail.pageItem[i].top = (i*64)+110	

			if i == 4 then
				Detail.pageItem[i]:setFocus()
			else 
				Detail.pageItem[i]:unsetFocus()
			end
		end

		Detail.currentPageItem = 4
		Detail.pageItem[Detail.currentPageItem]:setData()
	else
		return false
	end
end


function Detail.paintContentBackground(imageUp, imageLeft, imageBackground, imageRigth, imageDown)

	if type(imageBackground) == "string" then
		local imageBackground = canvas:new(imageBackground)
	else
		local imageBackground = imageBackground
	end	

	if type(imageUp) == "string" then
		local imageUp = canvas:new(imageUp)
	else
		local imageUp = imageUp
	end
	if type(imageDown) == "string" then
		local imageDown = canvas:new(imageDown)
	else
		local imageDown = imageDown
	end
	if type(imageLeft) == "string" then
		local imageLeft = canvas:new(imageLeft)
	else
		local imageLeft = imageLeft
	end
	if type(imageRigth) == "string" then
		local imageRigth = canvas:new(imageRigth)
	else
		local imageRigth = imageRigth
	end

	canvas:compose(371,299,imageBackground)
	canvas:compose(382,300,imageUp)
	canvas:compose(374,299,imageLeft)
	canvas:compose(679,306,imageRigth)
	canvas:compose(384,486,imageDown)
end

function Detail.handler(key)

	if (key == 'RED'  or (key == 'CURSOR_RIGHT' and focused == 4) or (key == 'CURSOR_LEFT' and focused == 2)) then
		--Main
		Main.state = Main
		mainMenu[focused]:unsetFocus()
		focused = 1
		mainMenu[focused]:setFocus()
		Main.showInit()
	
	elseif (key == 'GREEN' or (key == 'CURSOR_RIGHT' and focused == 1) or (key == 'CURSOR_LEFT' and focused == 3)) then
		--Paperwork
		Main.state = Paperwork
		mainMenu[focused]:unsetFocus()
		focused = 2
		mainMenu[focused]:setFocus()
		Paperwork.init()
	
	
	elseif (key == 'YELLOW' or (key == 'CURSOR_RIGHT' and focused == 2) or (key == 'CURSOR_LEFT' and focused == 4)) then
		--Directory
		Main.state = Directory
		mainMenu[focused]:unsetFocus()
		focused = 3
		mainMenu[focused]:setFocus()
		Directory.init()
	
	elseif (key == 'BLUE' or (key == 'CURSOR_RIGHT' and focused == 3) or (key == 'CURSOR_LEFT' and focused == 1)) then
		--Calendar
		Main.state = Calendar
		mainMenu[focused]:unsetFocus()
		focused = 4
		mainMenu[focused]:setFocus()
		Calendar.init()
		
	--[[--------------
	
	elseif (key == 'CURSOR_RIGHT' and focused == 4) then
		print('###################################################################')
		print(Detail.section)
		Main.state = Main
		mainMenu[focused]:unsetFocus()
		focused = 1
		mainMenu[focused]:setFocus()
		Main.showInit()
		
	----------------]]
	
	elseif (key == 'CURSOR_UP') then
			
		if (Detail.currentPageItem == 1) then 
				
			--Detail.pageItem[1]:unsetFocus()
			--Detail.pageItem[#Detail.pageItem]:setFocus()
			--Detail.pageItem[#Detail.pageItem]:setData()
			--Detail.currentPageItem = #Detail.pageItem
			Detail.previousPage()
					
		else
				
			Detail.pageItem[Detail.currentPageItem]:unsetFocus()
			Detail.pageItem[Detail.currentPageItem-1]:setFocus()
			Detail.pageItem[Detail.currentPageItem-1]:setData()
			Detail.currentPageItem = Detail.currentPageItem-1
			
		end
				
				
	elseif (key == 'CURSOR_DOWN') then
			
				
		if (Detail.currentPageItem == #Detail.pageItem) then 
		
			--Detail.pageItem[#Detail.pageItem]:unsetFocus()
			--Detail.pageItem[1]:setFocus()
			--Detail.pageItem[1]:setData()
			--Detail.currentPageItem = 1
			Detail.nextPage()

			
		else
		
			Detail.pageItem[Detail.currentPageItem]:unsetFocus()
			Detail.pageItem[Detail.currentPageItem+1]:setFocus()
			Detail.pageItem[Detail.currentPageItem+1]:setData()
			Detail.currentPageItem = Detail.currentPageItem+1
		
		end	
	--[[	
	elseif (key == 'CURSOR_RIGHT') then
	
		Detail.nextPage()
			
	elseif (key == 'CURSOR_LEFT') then
		if Detail.previousPage() == false then
			if Detail.section == Directory then
				Main.state = Detail.section
				focused = 3
				canvas:compose(0,0,images.background)
				mainMenu:refreshMenu()
				Directory.init()
			else
				Main.state = Main
				focused = 1
				canvas:compose(0,0,images.background)
				mainMenu:refreshMenu()
				Main.showInit()
			end
		end	
	--]]	
	elseif (key == 'ENTER' or key == 'OK') then

	end
	canvas:flush()

end

