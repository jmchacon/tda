
images = {}

images.b_texto = canvas:new('../resources/images/buttons/b_texto.png')
images.b_texto2 = canvas:new('../resources/images/buttons/b_texto2.png')
images.b_green = canvas:new('../resources/images/buttons/b_texto_verde.png')
images.b_red = canvas:new('../resources/images/buttons/b_texto_rojo.png')
images.b_blue = canvas:new('../resources/images/buttons/b_texto_azul.png')
images.background = canvas:new('../resources/images/main_Menu.png')
images.button_inicio = canvas:new('../resources/images/buttons/b_inicio.png')
images.button_inicio2 = canvas:new('../resources/images/buttons/b_inicio2.png')
images.button_tramites = canvas:new('../resources/images/buttons/b_tramites.png')
images.button_tramites2 = canvas:new('../resources/images/buttons/b_tramites2.png')
images.button_directorio = canvas:new('../resources/images/buttons/b_direct.png')
images.button_directorio2 = canvas:new('../resources/images/buttons/b_direct2.png')
images.button_agenda = canvas:new('../resources/images/buttons/b_agenda.png')
images.button_agenda2 = canvas:new('../resources/images/buttons/b_agenda2.png')
images.iconGobierno = canvas:new('../resources/images/gobierno.png')

images.arrowDown = canvas:new('../resources/images/arrowDown.png')
images.arrowUp = canvas:new('../resources/images/arrowUp.png')
--images.nextPage = canvas:new('../resources/images/exitButton.png')

-- Content images:
images.contentBackground = canvas:new('../resources/images/libertad2.png')

images.barLeftRed = canvas:new('../resources/images/redBarLeft.png')
images.barUpRed = canvas:new('../resources/images/redBarUpper.png')
images.barRigthRed = canvas:new('../resources/images/redBarRigth.png')
images.barDownRed = canvas:new('../resources/images/redBarLower.png')

images.barLeftGreen = canvas:new('../resources/images/greenBarLeft.png')
images.barUpGreen = canvas:new('../resources/images/greenBarUpper.png')
images.barRigthGreen = canvas:new('../resources/images/greenBarRigth.png')
images.barDownGreen = canvas:new('../resources/images/greenBarLower.png')

images.barLeftYellow = canvas:new('../resources/images/yellowBarLeft.png')
images.barUpYellow = canvas:new('../resources/images/yellowBarUpper.png')
images.barRigthYellow = canvas:new('../resources/images/yellowBarRigth.png')
images.barDownYellow = canvas:new('../resources/images/yellowBarLower.png')

images.barLeftBlue = canvas:new('../resources/images/blueBarLeft.png')
images.barUpBlue = canvas:new('../resources/images/blueBarUpper.png')
images.barRigthBlue = canvas:new('../resources/images/blueBarRigth.png')
images.barDownBlue = canvas:new('../resources/images/blueBarLower.png')


mainMenu = {}

mainMenu[1] = Button:new():setBounds(18,100,87,36,false,images.button_inicio,images.button_inicio2,'Gobierno en Linea coloca al alcance de todas y todos  una gama de trámites y servicios destinados a satisfacer las necesidades de nuestros ciudadanos, integrando a su plataforma registros que facilitan la búsqueda de este tipo de información.',386,310)
mainMenu[2] = Button:new():setBounds(105,100,87,36,false,images.button_tramites,images.button_tramites2,'',386,310)
mainMenu[3] = Button:new():setBounds(192,100,87,36,false,images.button_directorio,images.button_directorio2,'',386,310)
mainMenu[4] = Button:new():setBounds(279,100,87,36,false,images.button_agenda ,images.button_agenda2 ,'',386,310,"red",371,295)

mainMenu.baseX = 100
mainMenu.baseY = 190

paperFocused = 1
dirFocused = 1
detFocused = 1

function mainMenu.getFocused(var)
	for i, v in ipairs(var) do
	
		if v.focus then
			focused, jj  = i, i
			return i
		end
	end
end

function mainMenu.refreshMenu(var)
	for i, v in ipairs(var) do
		if v.focus then
			var[i]:setFocus()
		else 
			var[i]:unsetFocus()			
		end
	end
end 

function mainMenu.paintMainBackground()
	canvas:compose(0,0,images.background)
	canvas:compose(mainMenu.baseX,mainMenu.baseY,images.iconGobierno)
	--canvas:compose(105,520,images.nextPage)
end

function mainMenu.paintTextArea()
	Detail.paintContentBackground(images.barUpRed,images.barLeftRed, images.contentBackground,images.barRigthRed, images.barDownRed)
	canvas:flush()
end

function mainMenu.paintBackgroundDetail()
	canvas:compose(0,0,images.background)
	canvas:compose(180,145,images.arrowUp)
	canvas:compose(180,440,images.arrowDown)
	--canvas:compose(105,520,images.nextPage)
	mainMenu:refreshMenu()
end

function mainMenu.showSprite()
	Sprite.init(70,115,320,5,20)
	Sprite.paint()
end

function mainMenu.hideSprite()
	Sprite.hide()
end
