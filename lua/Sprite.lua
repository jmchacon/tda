Sprite = {}

function Sprite.init(pos_X, pos_Y, timeStep, totalSteps, cicles)

	Sprite.pos_X = pos_X --posicion en X del icono animado
	Sprite.pos_Y = pos_Y --posicion en Y del icono animado
	Sprite.imgPath = '../resources/images/sprite/icono1/icono' --ubicación de las imagenes para el sprite
	Sprite.timeStep = timeStep --retardo de los intervalos en milisegundos
	Sprite.totalSteps = totalSteps --ancho total de la imagen sprite
	Sprite.count = 1
	Sprite.cicles = cicles
	Sprite.load()
end

function Sprite.load()

	Sprite.imgs = {}
	for i = 1, Sprite.totalSteps do
		Sprite.imgs[i] = canvas:new(Sprite.imgPath .. i .. '.png')
	end
end

function Sprite.paint()
	
	canvas:compose(Sprite.pos_X, Sprite.pos_Y, Sprite.imgs[Sprite.count])
	canvas:flush()
	
	if Sprite.totalSteps > Sprite.count then
		Sprite.timer = event.timer(Sprite.timeStep, Sprite.paint)
		Sprite.count = Sprite.count + 1
	else
		Sprite.cicles = Sprite.cicles - 1
		if Sprite.cicles ~= 0 then
			Sprite.count = 1
			Sprite.paint()
		end
	end
end

function Sprite.hide()

	if Sprite.timer then
		Sprite.timer()
		Sprite.timer = nil
	end

	canvas:attrColor(0,0,0,0)
	canvas:clear()
end

