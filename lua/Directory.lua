
Directory = {}

Directory.ciuda1 = canvas:new ('../resources/images/buttons/direc_ciuda1.png')
Directory.ciuda2 = canvas:new ('../resources/images/buttons/direc_ciuda2.png')
Directory.ejecu1 = canvas:new ('../resources/images/buttons/direc_ejecu1.png')
Directory.ejecu2 = canvas:new ('../resources/images/buttons/direc_ejecu2.png')
Directory.elect1 = canvas:new('../resources/images/buttons/direc_elect1.png')
Directory.elect2 = canvas:new('../resources/images/buttons/direc_elect2.png')
Directory.estadal1 = canvas:new('../resources/images/buttons/direc_estadal1.png')
Directory.estadal2 = canvas:new('../resources/images/buttons/direc_estadal2.png')
Directory.judicial1 = canvas:new('../resources/images/buttons/direc_judicial1.png')
Directory.judicial2 = canvas:new('../resources/images/buttons/direc_judicial2.png')
Directory.legis1 = canvas:new('../resources/images/buttons/direc_legis1.png')
Directory.legis2 = canvas:new('../resources/images/buttons/direc_legis2.png')
Directory.seguri1 = canvas:new('../resources/images/buttons/direc_seguri1.png')
Directory.seguri2 = canvas:new('../resources/images/buttons/direc_seguri2.png')

function Directory.setupData()
	Poder = {}
	Poder[1] = Button:new():setBounds(90, 150, 187, 45, true, Directory.ciuda1, Directory.ciuda2, 'Encargado de promover la educación como proceso creador de la ciudadanía, así como la solidaridad, la libertad, la democracia, la responsabilidad social y el trabajo.', 386, 310, content.directory[1])

	Poder[2] = Button:new():setBounds(90, 200, 187, 45, false, Directory.ejecu1, Directory.ejecu2, 'Ejercido por el Presidente de la República, Vicepresidente o Vicepresidenta Ejecutivo, Ministros o Ministras y demás funcionarios.', 386, 310, content.directory[2])

	Poder[3] = Button:new():setBounds(90, 250, 187, 45, false, Directory.elect1, Directory.elect2, 'Ejerce por órgano del Consejo Nacional Electoral, como ente rector, y como órganos subordinados a este, por la Junta Nacional Electoral, la Comisión de Registro Civil y Electoral y la Comisión de Participación Política y Financiamiento', 386, 310, content.directory[3])

	Poder[4] = Button:new():setBounds(90, 300, 187, 45, false, Directory.estadal1, Directory.estadal2, 'Información detallada de las instituciones públicas a nivel estadal', 386, 310, content.directory[4])

	Poder[5] = Button:new():setBounds(90, 350, 187, 45, false, Directory.judicial1, Directory.judicial2, 'El Poder Judicial es el encargado de administrar la justicia emanada de los ciudadanos y se imparte en nombre de la República por autoridad de la ley,esta constituido por el Tribunal Supremo de Justicia.', 386, 310, content.directory[5])

	Poder[6] = Button:new():setBounds(90, 400, 187, 45, false, Directory.legis1, Directory.legis2, 'El Poder Legislativo es el encargado de la formación, discusión y cumplimiento de las leyes federales', 386, 310, content.directory[6])

	Poder[7] = Button:new():setBounds(90, 450, 187, 45, false, Directory.seguri1, Directory.seguri2, 'Información detallada de las instituciones de seguridad del estado', 386, 310, content.directory[7])

end

function Directory.init()

	if Poder == nil then
		Directory.setupData()
	end

	canvas:compose(0,0,images.background)
	--canvas:compose(105,520,images.nextPage)
	mainMenu:refreshMenu()
	mainMenu.refreshMenu(Poder) 
	canvas:flush()

end

function Directory.handler(key)

	if (key == 'RED') then
		--Main
		Main.state = Main
		mainMenu[focused]:unsetFocus()
		focused = 1
		mainMenu[focused]:setFocus()
		Main.showInit()
	
	elseif (key == 'GREEN' or key == 'CURSOR_LEFT') then
		--Paperwork
		Main.state = Paperwork
		mainMenu[focused]:unsetFocus()
		focused = 2
		mainMenu[focused]:setFocus()
		Paperwork.init()
		
	elseif (key == 'YELLOW') then
		--Directory
		Main.state = Directory
		mainMenu[focused]:unsetFocus()
		focused = 3
		mainMenu[focused]:setFocus()
		Directory.init()

	elseif (key == 'BLUE' or key == 'CURSOR_RIGHT') then
		--Calendar
		Main.state = Calendar
		mainMenu[focused]:unsetFocus()
		focused = 4
		mainMenu[focused]:setFocus()
		Calendar.init()
	
	elseif (key == 'CURSOR_UP') then
		if (dirFocused == 1) then 
			Poder[1]:unsetFocus()
			Poder[#Poder]:setFocus()
			dirFocused = #Poder
				
		else
			Poder[dirFocused]:unsetFocus()
			Poder[dirFocused-1]:setFocus()
			dirFocused = dirFocused-1
		end
						
	elseif (key == 'CURSOR_DOWN') then
		if (dirFocused == #Poder) then 
			Poder[#Poder]:unsetFocus()
			Poder[1]:setFocus()
			dirFocused = 1
			
		else
			Poder[dirFocused]:unsetFocus()
			Poder[dirFocused+1]:setFocus()
			dirFocused = dirFocused+1	
		end		
	--[[
	elseif (key == 'CURSOR_LEFT') then
		Main.showInit()
		Main.state = Main
	--]]
	elseif (key == 'ENTER' or key == 'OK') then

		Main.state = Detail
		Detail.initDetail(Poder[dirFocused].data, Directory)
			
	end
	canvas:flush()
end

