Button = {}

function Button:new()
	
	instance = {}
	setmetatable(instance, self)
	self.__index = self
	return instance
end

function Button:setBounds(left, top, width, height, focus, imagePath, imagefocusPath, summary,leftsummary, topsummary, data) 
	--mainMenu[3] = Button:new():setBounds(192,101,87,36,false,images.button_directorio,images.button_directorio2,'',386,310,"")
	self.left = left
	self.top = top
	self.width = width
	self.height = height
	self.focus = focus

	if type(imagePath) == "string" then
		self.image = canvas:new(imagePath)
	else
		self.image = imagePath
	end

	if type(imagefocusPath) == "string" then
		self.imagefocus = canvas:new(imagefocusPath)
	else
		self.imagefocus = imagefocusPath
	end

	self.summary = summary
	self.leftsummary = leftsummary
	self.topsummary = topsummary

	self.imgbackFocus = imgbackFocus

	--self.backleft = backleft
	--self.backtop = backtop
	self.data = data

	return self
end
function Button:paintAreaButton(self)
	canvas:compose(self.left,self.top,self.image )
	
end
function Button:paintTextSummary(self)
	canvas:compose(self.leftsummary,self.topsummary,self.summary )	
end

function Button:clearSummary()

	canvas:attrColor(0,0,0,0)
	canvas:clear (350, 300, 300, 100)
	canvas:flush()
end


function Button:Justify(s)

	out = {}
	out[1] = ''
	h=1

	for w in string.gmatch(s, "[^%s]*") do
		d = out[h]
		tamano = string.len(d)
		
		if  tamano < 30 then
			out[h] = out[h]..' '..w
		else
			h = h + 1		
			out[h] = ''
		end		
	end
end

function Button:setSummary()
	Button.paintBorder()
	canvas:attrFont('Tiresias', 14)
	self:Justify(self.summary)
	y = self.topsummary+8
	
	canvas:attrColor(0,0,0,255)
	for i, v in ipairs(out) do
		canvas:drawText(self.leftsummary+5,y,out[i])
		y = y + 15
	end
	canvas:attrColor(0,0,0,0)
end

function Button:setFocus()
	
	self.focus = true
	self.image:attrColor(0,0,0,0)
	canvas:compose(self.left, self.top, self.imagefocus)
	self:setSummary()
	
end

function Button:unsetFocus()
	
	self.focus = false
	self.image:attrColor(0,0,0,0)
	canvas:compose(self.left, self.top, self.image)
end

function Button.paintBorder()
	if mainMenu[1].focus then
		Detail.paintContentBackground(images.barUpRed, images.barLeftRed, images.contentBackground, images.barRigthRed, images.barDownRed)
	elseif mainMenu[2].focus then
		Detail.paintContentBackground(images.barUpGreen, images.barLeftGreen, images.contentBackground, images.barRigthGreen, images.barDownGreen)
	elseif mainMenu[3].focus then
		Detail.paintContentBackground(images.barUpYellow, images.barLeftYellow, images.contentBackground, images.barRigthYellow, images.barDownYellow)
	elseif mainMenu[4].focus then
		Detail.paintContentBackground(images.barUpBlue, images.barLeftBlue, images.contentBackground, images.barRigthBlue, images.barDownBlue)
	end
end

