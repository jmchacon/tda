Calendar = {}

function Calendar.init()
	
--	mainMenu.paintBackground()
--	mainMenu.refreshMenu(Perfil)
	Main.state = Detail
	Detail.initDetail(content.calendar, Calendar)
	
end


function Calendar.handler(key)

	if (key == 'RED') then
		--Main
		Main.state = Main
		mainMenu[focused]:unsetFocus()
		focused = 1
		mainMenu[focused]:setFocus()
		Main.showInit()
	
	
	elseif (key == 'GREEN') then
		--Paperwork
		Main.state = Paperwork
		mainMenu[focused]:unsetFocus()
		focused = 2
		mainMenu[focused]:setFocus()
		Paperwork.init()
		
	elseif (key == 'YELLOW') then
		--Directory
		Main.state = Directory
		mainMenu[focused]:unsetFocus()
		focused = 3
		mainMenu[focused]:setFocus()
		Directory.init()

	elseif (key == 'BLUE') then
		--Calendar
		Main.state = Calendar
		mainMenu[focused]:unsetFocus()
		focused = 4
		mainMenu[focused]:setFocus()
		Calendar.init()
	end

end
