return {
  [1] = {
  ["site"] = "Saime Amazonas",
  ["name"] = "Cedulació Amazonas",
  ["address"] = "Amazonas",
  ["phone"] = "Municipio Atures,",
  ["leader"] = "Municipio Atures, parroquia Fernando Girón Tovar, Av. 23 de Enero, frente al CICPC",
}
,
  [2] = {
  ["site"] = "Saime Amazonas",
  ["name"] = "Cedulació Amazonas",
  ["address"] = "Amazonas",
  ["phone"] = "municipio Atures,",
  ["leader"] = "Municipio Atures, parroquia Luis Alberto Gómez, Av. Orinoco, frente al estación de servicios La Florida.",
}
,
  [3] = {
  ["site"] = "Saime Anzóategui",
  ["name"] = "Cedulación Anzóategui",
  ["address"] = "Anzoateguí",
  ["phone"] = "Municipio Simón Rodríguez",
  ["leader"] = "Municipio Simón Rodríguez, parroquia El Tigre, Plaza Bolívar, PSUV.",
}
,
  [4] = {
  ["site"] = "Mercal Aragua",
  ["name"] = "Mercal Aragua",
  ["address"] = "Aragua",
  ["phone"] = "Municipio Girardot",
  ["leader"] = "Municipio Girardot, parroquia Madre María de San José, hospital militar al lado de 42 brigada blindada de paracaidista",
}
,
  [5] = {
  ["site"] = "Mercal Aragua",
  ["name"] = "Mercal Aragua",
  ["address"] = "Aragua",
  ["phone"] = "Municipio Girardot",
  ["leader"] = "Municipio Girardot, parroquia madre Madre María de San José, callejón Girardot          frente a Locatel de la Miranda",
}
,
  [6] = {
  ["site"] = "Mercal Barinas",
  ["name"] = "Mercal Barinas",
  ["address"] = "Barinas",
  ["phone"] = "Municipio Barinas",
  ["leader"] = "Municipio Barinas, parroquia Alto Barinas, bodega móvil apartamentos Ciudad Tabacare sector b",
}
,
  [7] = {
  ["site"] = "Mercal Yaracuy",
  ["name"] = "Mercal Yaracuy",
  ["address"] = "Yaracuy",
  ["phone"] = "Municipio San Felipe",
  ["leader"] = "Municipio San Felipe, parroquia San Felipe, Av. Libertador, Entre Calles 4 y 5, esquina de la frutería.",
}
,
}
