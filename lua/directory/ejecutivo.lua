return {
  [1] = {
  ["site"] = "http://www.vicepresidencia.gob.ve",
  ["name"] = "Vicepresidencia de la República Bolivariana de Venezuela",
  ["address"] = "Avenida Urdaneta, Esquina de Carmelitas. - Caracas - Gran Caracas - Venezuela",
  ["phone"] = "",
  ["leader"] = "",
  ["inode"] = "142550",
}
,
  [2] = {
  ["site"] = "http://www.mpps.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Salud",
  ["address"] = "Edif.Sur Centro Simón Bolívar, Caracas, Venezuela.",
  ["phone"] = "",
  ["leader"] = "Eugenia Sader Castellanos",
  ["inode"] = "144578",
}
,
  [3] = {
  ["site"] = "http://www.mpprij.gob.ve",
  ["name"] = "Ministerio del Poder Popular para Relaciones Interiores y Justicia",
  ["address"] = "Avenida Urdaneta, esquina Platanal, Edificio Sede - Caracas - Venezuela",
  ["phone"] = "",
  ["leader"] = "Tarek El Aissami",
  ["inode"] = "144581",
}
,
  [4] = {
  ["site"] = "http://www.mppeu.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Educación Universitaria",
  ["address"] = "Av. Universidad. Esq. El Chorro, torre Ministerial, Pisos: 1-7, Caracas - Venezuela",
  ["phone"] = "",
  ["leader"] = "Yadira Cordova",
  ["inode"] = "144582",
}
,
  [5] = {
  ["site"] = "http://www.mpcomunas.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para las Comunas y Protección Social",
  ["address"] = "Av.Nueva Granada Edificio INCES, Sede Apartado 10340 - Caracas 1040- Venezuela",
  ["phone"] = "",
  ["leader"] = "Isis Ochoa",
  ["inode"] = "144583",
}
,
  [6] = {
  ["site"] = "http://www.mf.gov.ve",
  ["name"] = "Ministerio del Poder Popular de Planificación y Finanzas",
  ["address"] = "Av. Urdaneta, con esquina de Carmelitas. Edificio Sede del Ministerio del Poder Popular de Planificación y Finanzas. Caracas - Venezuela 1010",
  ["phone"] = "",
  ["leader"] = "Jorge Giordani",
  ["inode"] = "144590",
}
,
  [7] = {
  ["site"] = "http://www.mppee.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Energía Eléctrica",
  ["address"] = "Edif. Ministerio del Poder Popular para la Energía Eléctrica, Av. Vollmer, Urb. San Bernardino, Municipio Libertador, Código Postal 1010, Distrito Capital.",
  ["phone"] = "0212 . 501. 10. 01",
  ["leader"] = "Alí Rodríguez Araque",
  ["inode"] = "144599",
}
,
  [8] = {
  ["site"] = "http://www.ministeriodelacultura.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Cultura",
  ["address"] = "Av. Panteón Foro Libertador, Edif. Archivo General de la NaciónPB.",
  ["phone"] = "0426-514-77-40/ 0212-339-23-04  - atc@mincultura.gob.ve",
  --phone errado
  ["leader"] = "Pedro Calzadilla",
  ["inode"] = "144603",
}
,
  [9] = {
  ["site"] = "http://www.me.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Educación",
  ["address"] = "Esq. de Salas a Caja de Agua, Edif. Sede del MPPE Parroquia Altagracia, Caracas. Dto. Capital, Venezuela",
  ["phone"] = "atencionalciudadano@me.gob.ve",
  ["leader"] = "Maryann Hanson Flores",
  ["inode"] = "144606",
}
,
  [10] = {
  ["site"] = "http://www.mvh.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para Vivienda y Hábitat",
  ["address"] = "Avenida Francisco de Miranda, Torre del Ministerio del Poder Popular para Vivienda y Hábitat, antigua sede de INAVI, Municipio Chacao, Edo. Miranda, Venezuela.",
  ["phone"] = "",
  ["leader"] = "Ricardo Molina",
  ["inode"] = "144586",
}
,
  [11] = {
  ["site"] = "http://www.mincomercio.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para el Comercio",
  ["address"] = "Av. Lecuna, Torre Oeste de Parque Central, Entrada por el Nivel Lecuna.",
  ["phone"] = "(0212) 509.67.54",
  ["leader"] = "Richard Canán",
  ["inode"] = "144587",
}
,
  [12] = {
  ["site"] = "http://www.minpptrass.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para el Trabajo y Seguridad Social",
  ["address"] = "Centro Simón Bolívar. Torre Sur. Piso 5 Caracas Distrito Capital Venezuela",
  ["phone"] = "",
  ["leader"] = "María Cristina Iglesias",
  ["inode"] = "144588",
}
,
  [13] = {
  ["site"] = "http://www.presidencia.gob.ve",
  ["name"] = "Ministerio del Poder Popular para el Despacho de la Presidencia",
  ["address"] = "Final Avenida Urdaneta, Esq. de Bolero, Palacio de Miraflores, Caracas, Distrito Capital.",
  ["phone"] = "dggcomunicacional@presidencia.gob.ve",
  ["leader"] = " Erika Farias",
  ["inode"] = "144589",
}
,
  [14] = {
  ["site"] = "http://www.mintur.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para el Turismo",
  ["address"] = "Av. Francisco de Miranda con Av.ppal de la Floresta, Edif.Mintur, (Frente al Colegio Universitario de Caracas), Municipio Chacao, Edo.Bolivariano de Miranda, Caracas, Venezuela",
  ["phone"] = "",
  ["leader"] = " Alejandro Fleming",
  ["inode"] = "144596",
}
,
  [15] = {
  ["site"] = "http://www.mibam.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Industria",
  ["address"] = "Av. La Estancia, Torre las Mercedes, piso 9, Urb. Chuao, Caracas - Venezuela",
  ["phone"] = "",
  ["leader"] = "Ricardo Menéndez",
  ["inode"] = "144610",
}
,
  [16] = {
  ["site"] = "http://www.inj.gov.ve/",
  ["name"] = "Ministerio del Poder Popular para la Juventud",
  ["address"] = "Final de la Av. Lucuna. Parque Central, torre OESTE. Piso 46.  Parroquia San Agustín. Caracas - Venezuela",
  ["phone"] = "0212-5777070 	Email:   nacional@inj.gov.ve",
  ["leader"] = "",
  ["inode"] = "144592",
}
,
  [17] = {
  ["site"] = "http://www.mat.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Agricultura y Tierras",
  ["address"] = "Av. Urdaneta entre esquina Platanal a Candilito a media cuadra de la Plaza la candelaria 	Parroquia la Candelaria - Caracas - Venezuela",
  ["phone"] = "0-800 AGRICUL (2474285)",
  ["leader"] = "Juan Carlos Loyo Hernández",
  ["inode"] = "144593",
}
,
  [18] = {
  ["site"] = "http://www.mre.gob.ve",
  ["name"] = "Ministerio del Poder Popular para Relaciones Exteriores",
  ["address"] = "Avenida Urdaneta, Torre MRE, al lado del Correo de Carmelitas. - Caracas - Gran Caracas - Venezuela",
  ["phone"] = "",
  ["leader"] = "Nicolás Maduro Moros",
  ["inode"] = "144595",
}
,
  [19] = {
  ["site"] = "http://www.mibam.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para las Industrias Básicas y Minería",
  ["address"] = "Av. La Estancia, Torre las Mercedes, piso 9, Urb. Chuao, Caracas - Venezuela",
  ["phone"] = "",
  ["leader"] = "José Khan Fernández",
  ["inode"] = "144597",
}
,
  [20] = {
  ["site"] = "http://minpi.gob.ve",
  ["name"] = "Ministerio del Poder Popular para los Pueblos Indígenas",
  ["address"] = "Av. Universidad Esq. Traposos, Antiguo Edificio Sudeban, Piso 8 Caracas",
  ["phone"] = "atencionalindigena@minpi.gob.ve",
  ["leader"] = "Nicia Maldonado ",
  ["inode"] = "144598",
}
,
  [21] = {
  ["site"] = "http://www.minmujer.gob.ve",
  ["name"] = "Ministerio del Poder Popular para la Mujer y la Igualdad de Género",
  ["address"] = "Esq. Jesuítas, Torre Bandagro, P-2, Altagracia, Caracas",
  ["phone"] = "minmujer@minmujer.gob.ve",
  ["leader"] = "Nancy Pérez Sierra",
  ["inode"] = "144600",
}
,
  [22] = {
  ["site"] = "http://www.mindeporte.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para el Deporte",
  ["address"] = "Avenida Teheran, Sede Principal del Ministerio del Poder Popular Para el Deporte,  S/N, Urbanización Montalbán, La Vega, Caracas, Distrito Capital.",
  ["phone"] = "",
  ["leader"] = " Héctor Rodríguez Castro",
  ["inode"] = "144601",
}
,
  [23] = {
  ["site"] = "www.minpal.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Alimentación",
  ["address"] = "Av. Andrés Bello, Edificio Las Fundaciones, Caracas",
  ["phone"] = "",
  ["leader"] = "Carlos Osorio Zambrano",
  ["inode"] = "144602",
}
,
  [24] = {
  ["site"] = "http://www.minci.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para la Comunicación y la Información",
  ["address"] = "Av. Universidad, Torre Ministerial, pisos 9 y 10. Caracas - Dtto. Capital",
  ["phone"] = "",
  ["leader"] = "Andrés Izarra",
  ["inode"] = "144605",
}
,
  [25] = {
  ["site"] = "http://www.mtc.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para Transporte  Terrestre",
  ["address"] = "Av. Francisco de Miranda, torre MTC, municipio Chacao, estado Miranda.",
  ["phone"] = "+58 (212) 201-50-12 / 41-08 / 43-25  Fax: +58 (212) 201-59-40",
  ["leader"] = "Juan García Toussantt",
  ["inode"] = "144608",
}
,
  [26] = {
  ["site"] = "http://www.mpptaa.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para Transporte Aéreo y Acuático (MPPTAA)",
  ["address"] = "No disponible",
  ["phone"] = "No disponible",
  ["leader"] = "Elsa Gutiérrez",
  ["inode"] = "144609",
}
,
  [27] = {
  ["site"] = "www.menpet.gob.ve/",
  ["name"] = "Ministerio del Poder Popular de Petróleo y Minería",
  ["address"] = "Av. Libertador, con Calle Empalme, Edf. Mempert, La Campiña, Caracas",
  ["phone"] = "atencionalpublico@menpet.gob.ve",
  ["leader"] = "Rafael Ramírez Carreño",
  ["inode"] = "144611",
}
,
  [28] = {
  ["site"] = "http://www.minamb.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para el Ambiente",
  ["address"] = "Centro Simon Bolivar, Torre Sur Plaza Caracas",
  ["phone"] = "",
  ["leader"] = "Alejandro Hicther",
  ["inode"] = "144612",
}
,
  [29] = {
  ["site"] = "http://www.mindefensa.gov.ve/",
  ["name"] = "Ministerio del Poder Popular para la Defensa",
  ["address"] = "Edf. Sede del Ministerio de La Defensa, Fuerte Tiuna, El Valle, Caracas Teléfono:",
  ["phone"] = "",
  ["leader"] = "Henry Rangel Silva",
  ["inode"] = "144700",
}
,
  [30] = {
  ["site"] = "http://www.mcti.gob.ve/",
  ["name"] = "Ministerio del Poder Popular para Ciencia, Tecnología e Innovación",
  ["address"] = "Av. Universidad. Esquina El Chorro. Torre Ministerial. La Hoyada, Caracas.",
  ["phone"] = "Av. Universidad. Esquina El Chorro. Edficio anexo A. J. Castillo. Nivel Mezzanina (parte trasera de la Torre Ministerial).",
  ["leader"] = "Jorge Alberto Arreaza",
  ["inode"] = "148393",
}
,
  [31] = {
  ["site"] = "www.presidencia.gob.ve/",
  ["name"] = "Presidencia de la República Bolivariana de Venezuela",
  ["address"] = "Final Avenida Urdaneta, Esq. de Bolero, Palacio de Miraflores, Caracas, Distrito Capital.",
  ["phone"] = "dasociales@presidencia.gob.ve",
  ["leader"] = "Hugo Rafael Chávez Frías",
  ["inode"] = "148403",
}
,
}
