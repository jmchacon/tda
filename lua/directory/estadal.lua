return {
  [1] = {
  ["site"] = "www.monagas.gob.ve",
  ["name"] = "Gobernación del Estado Monagas",
  ["address"] = "Calle Monagas, Frente a la Plaza Bolívar Maturín - Monagas",
  ["phone"] = "",
  ["leader"] = "José Gregorio Briceño",
  ["inode"] = "144844",
}
,
  [2] = {
  ["site"] = "http://www.deltamacuro.gob.ve",
  ["name"] = "Gobernació del Estado Delta Amacuro",
  ["address"] = "Calle Bolívar, Casa de Gobierno Diagonal Plaza Bolívar. - Tucupita - Delta Amacuro",
  ["phone"] = "",
  ["leader"] = "Lizeta Hernández",
  ["inode"] = "144850",
}
,
  [3] = {
  ["site"] = "http://www.nuevaesparta.gob.ve/",
  ["name"] = "Gobernación del Estado Nueva Esparta",
  ["address"] = "Avenida Constitución, Palacio de Gobierno. - La Asunción - Nueva Esparta",
  ["phone"] = "",
  ["leader"] = "Morel Rodríguez Ávila",
  ["inode"] = "144838",
}
,
  [4] = {
  ["site"] = "",
  ["name"] = "Gobernación del Estado Cojedes",
  ["address"] = "Calle Manrique con calle Sucre, frente a la plaza Bolívar. - San Carlos - Cojedes",
  ["phone"] = "",
  ["leader"] = "Teodoro Bolívar",
  ["inode"] = "144839",
}
,
  [5] = {
  ["site"] = "http://www.amazonas.gob.ve/",
  ["name"] = "Gobernación del Estado Amazonas",
  ["address"] = "Edif. Gobernación del Estado Amazonas, Av. Río Negro frente a la Plaza Bolívar. Puerto Ayacucho Estado Amazonas - Venezuela",
  ["phone"] = "",
  ["leader"] = "Liborio Guarulla",
  ["inode"] = "144840",
}
,
  [6] = {
  ["site"] = "http://www.lara.gob.ve/",
  ["name"] = "Gobernación del Estado Lara",
  ["address"] = "Carrera 19, Esquina de la Calle 23. - Barquisimeto - Lara -",
  ["phone"] = "",
  ["leader"] = "Henri Falcón",
  ["inode"] = "144845",
}
,
  [7] = {
  ["site"] = "www.miranda.gov.ve",
  ["name"] = "Gobernación del Estado Miranda",
  ["address"] = "Palacio de Gobierno frente a la Plaza Bolívar, Los Teques - Miranda",
  ["phone"] = "atencion.soberano@miranda.gob.ve 0212 – 3216705 / 0800 – 6472632",
  ["leader"] = "Henrique Capriles Radonski",
  ["inode"] = "144846",
}
,
  [8] = {
  ["site"] = "http://www.gobernaciondelzulia.gov.ve/",
  ["name"] = "Gobernación del Estado Zulia",
  ["address"] = "Palacio de Gobierno calle A-5 frente a Plaza Bolívar. Maracaibo. Zulia",
  ["phone"] = "",
  ["leader"] = "Pablo Pérez Álvarez",
  ["inode"] = "144849",
}
,
  [9] = {
  ["site"] = "http://www.carabobo.gob.ve",
  ["name"] = "Gobernación del Estado Carabobo",
  ["address"] = "Calle Páez entre Avenidas Montes de Oca y Díaz Moreno, Capitolio de Valencia.",
  ["phone"] = "",
  ["leader"] = " Henrique Salas Feo",
  ["inode"] = "144883",
}
,
  [10] = {
  ["site"] = "http://www.estadovargas.gob.ve",
  ["name"] = "Gobernación del Estado Vargas",
  ["address"] = "Av. Soublette, Casa Guipuzcoana, La Guaira, Edo. Vargas",
  ["phone"] = "",
  ["leader"] = "Jorge Luis García Carneiro ",
  ["inode"] = "144836",
}
,
  [11] = {
  ["site"] = "http://portal.falcon.gob.ve/",
  ["name"] = "Gobernación del Estado Falcón",
  ["address"] = "Calle Ampíes, Palacio de Gobierno. - Coro - Falcón",
  ["phone"] = "",
  ["leader"] = "Stella Marina Lugo de Montilla",
  ["inode"] = "144847",
}
,
  [12] = {
  ["site"] = "http://www.yaracuy.gob.ve",
  ["name"] = "Gobernación del Estado Yaracuy",
  ["address"] = "Frente a la plaza Bolívar San Felipe, Palacio de Gobierno, Estado Yaracuy",
  ["phone"] = "",
  ["leader"] = " Julio Cesar León Heredia",
  ["inode"] = "144848",
}
,
  [13] = {
  ["site"] = "http://gobiernodeanzoategui.com/",
  ["name"] = "Gobernación del Estado Anzoátegui",
  ["address"] = "Placio de Gobierno, P-4, Plaza Barrio, Barcelona",
  ["phone"] = "",
  ["leader"] = " Tarek William Saab",
  ["inode"] = "144837",
}
,
  [14] = {
  ["site"] = "http://www.apure.gob.ve/",
  ["name"] = "Gobernación del Estado Apure",
  ["address"] = "Edificio Administrativo del Ejecutivo, calle Comerico, entre Piar y Madariaga, San Fernando de Apure.",
  ["phone"] = "",
  ["leader"] = " Ramón Carrizales",
  ["inode"] = "144841",
}
,
  [15] = {
  ["site"] = "http://www.aragua.gob.ve/",
  ["name"] = "Gobernación del Estado Aragua",
  ["address"] = "Av. Miranda frente a la Plaza Bolí­var. Palacio de Gobierno. Maracay. Aragua",
  ["phone"] = "",
  ["leader"] = " Rafael Isea",
  ["inode"] = "144843",
}
,
  [16] = {
  ["site"] = "www.e-bolivar.gob.ve",
  ["name"] = "Gobernación del Estado Bolívar",
  ["address"] = "Calle Constitución, Palacio de Gobierno frente a la Plaza Bolívar. - Ciudad Bolívar - Bolívar -",
  ["phone"] = "",
  ["leader"] = "Francisco Rangel Gómez",
  ["inode"] = "144851",
}
,
  [17] = {
  ["site"] = "http://www.gobarinas.gob.ve/",
  ["name"] = "Gobernación del Estado Barinas",
  ["address"] = "Av. Marqués del Pumar, Palacio del Gobierno frente a la Plaza Bolívar. - Barinas - Barinas - Venezuela",
  ["phone"] = "",
  ["leader"] = "Adán Chávez Frías",
  ["inode"] = "144852",
}
,
  [18] = {
  ["site"] = "www.merida.gob.ve",
  ["name"] = "Gobernación del Estado Mérida",
  ["address"] = "Palacio de Gobierno, frente a la Plaza Bolívar, calle 23 entre Av. 3 y 4",
  ["phone"] = "",
  ["leader"] = "Marcos Díaz  Orellana",
  ["inode"] = "144853",
}
,
  [19] = {
  ["site"] = "www.edosucre.gov.ve",
  ["name"] = "Gobernación del Estado Sucre",
  ["address"] = "Calle Bolívar, Casa de Gobierno, Cumaná - Sucre",
  ["phone"] = "",
  ["leader"] = "Enrique Maestre",
  ["inode"] = "144881",
}
,
  [20] = {
  ["site"] = "www.portuguesa.gob.ve",
  ["name"] = "Gobernación del Estado Portuguesa",
  ["address"] = "Carrera 5ta entre calles 15 y 16, frente a la Plaza Bolívar ,Guanare - Portuguesa",
  ["phone"] = "",
  ["leader"] = "Wilmar Castro Soteldo",
  ["inode"] = "144882",
}
,
  [21] = {
  ["site"] = "http://tachira.gob.ve/",
  ["name"] = "Gobernación del Estado Táchira",
  ["address"] = "Residencia Oficial de Gobernadores, Carrera 17 calle 11 y 13 Barrio Obrero San Cristóbal / Carrera 10 entre calles 4 y 5, Casa de Gobierno, San Cristóbal - Estado Táchira",
  ["phone"] = "",
  ["leader"] = "Cesar Pérez Vivas",
  ["inode"] = "144884",
}
,
  [22] = {
  ["site"] = "http://www.guarico.gob.ve/",
  ["name"] = "Gobernación del Estado Guárico",
  ["address"] = "Avenida Monseñor Sendrea, Casa de Gobierno,Frente a la Plaza Bolívar. - San Juan De Los Morros - Guárico -",
  ["phone"] = "",
  ["leader"] = "Luis Enrique Gallardo",
  ["inode"] = "144885",
}
,
  [23] = {
  ["site"] = "http://www.gset.gob.ve/",
  ["name"] = "Gobernación del Estado Trujillo",
  ["address"] = "Av. Independencia, Palacio de Gobierno, Trujillo - Estado Trujillo",
  ["phone"] = "",
  ["leader"] = "Hugo Cesar Cabezas Bracamonte",
  ["inode"] = "144886",
}
,
  [24] = {
  ["site"] = "http://www.gdc.gob.ve",
  ["name"] = "Gobernación del Distrito Capital",
  ["address"] = "Esquina de Torre a Principal, Trente a la Plaza Bolivar, Casa de Gobierno del Distrito Capital. Parroquia Catedral. - Caracas",
  ["phone"] = "",
  ["leader"] = "Jacqueline Faría",
  ["inode"] = "148390",
}
,
}
