return {
  [1] = {
  ["site"] = "-Título Original debidamente firmado y sellado por el Registro Público correspondiente. -Original del Acta de grado o Notas Certificadas. -Copia de la Cédula de Identidad del titular.",
  ["name"] = "Legalización de Títulos Universitarios",
  ["address"] = "Lunes a Viernes de 8 a 12 y de 1:30 a 3:30",
  ["phone"] = "",
  ["leader"] = "Registro Principal",
  ["inode"] = "inode",
}
,
[2] = {
  ["site"] = "",
  ["name"] = "Permiso Definitivo para Empleo de Menores",
  ["address"] = "Final Av. FFAA, Esq. Isleño a Muerto, Edif. Isleño II,frente a la Iglesia San Agustín y Diagonal los Bomberos Nuevo Circo.  Caracas, Distrito Capital.",
  ["phone"] = "0212-542.03.59 / 0212-409.82.36 -EXT.26 / 0416-620.68.41",
  ["leader"] = "",
  ["inode"] = "",
}
,
[3] = {
  ["site"] = "-Copia Fondo Negro y dos nosmales del título legalizado -Copia acta de grado -Programa de asignaturas cursadas -Cedula o pasaporte -Constancia certificada de la universidad",
  ["name"] = "Reválida de Títulos Extranjeros",
  ["address"] = "Oficina de Reválidas de la institución seleccionada",
  ["phone"] = "",
  ["leader"] = "",
  ["inode"] = "",
}
,
[4] = {
  ["site"] = "-Fotocopia de la cédula. -Constancia de trabajo con emisión no mayor a tres meses. -3 Referencias personales con dirección, números telefónicos y copia de la cédula del emisor. -Copia de un recibo de servicio.",
  ["name"] = "Mi Casa bien equipada",
  ["address"] = "",
  ["phone"] = "",
  ["leader"] = "",
  ["inode"] = "",
}
,
}
